<?php
/**
 * Function to build the catalog administration form
 * @param array $form
 * @param array $form_state
 */
function vt_commerce_catalog_admin_settings($form, &$form_state) {
  // important variable to use
  $options_boolean = array( 'true' => 'True' , 'false' => 'False' );
  $option_max = range(0, 20, 1);
  unset($option_max[0]);

	// basic configuration
	$form['basic'] = array(
    '#type' => 'fieldset',
    '#weight' => 6,
    '#title' => t('Basic Configuration'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['basic']['vt_commerce_catalog_display_product_type'] = array(
    '#type' => 'select',
    '#multiple' => TRUE,
    '#options' => vt_commerce_catalog_get_product_display_types(),
    '#title' => t('Select the product display content type'),
    '#description' => t('You can select multiple content type by ctrl + click the selections.'),
    '#default_value' => variable_get('vt_commerce_catalog_display_product_type', array('product_display')),
  );

  $form['basic']['vt_commerce_catalog_api_limit'] = array (
    '#type' => 'textfield',
    '#title' => t('Set how many product per page'),
    '#description' => t('Please input numeric value only, this setting will be used to display product page and pagination'),
    '#default_value' => variable_get('vt_commerce_catalog_api_limit', '36'),
  );

  $form['basic']['vt_commerce_catalog_show_children'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show Taxonomy Children'),
    '#description' => t('By enabling this the module will add /all to all the catalog path rendered'),
    '#default_value' => variable_get('vt_commerce_catalog_show_children', TRUE),
  );

  $form['basic']['vt_commerce_catalog_theme'] = array(
    '#title' => t('Select Theme'),
    '#type' => 'select',
    '#options' => vt_commerce_catalog_read_dir('vt_commerce_catalog'),
    '#default_value' => variable_get('vt_commerce_catalog_basic_filter_theme', 'vt_commerce_catalog_default'),
  );
  $form['basic']['vt_commerce_catalog_display_type'] = array(
    '#type' => 'select',
    '#options' => array('grid' => t('Grid'), 'list' => t('List')),
    '#title' => t('Select the default product listing type'),
    '#description' => t('You can choose between grid or list display type'),
    '#default_value' => variable_get('vt_commerce_catalog_display_type', 'grid'),
  );

  $form['basic']['vt_commerce_catalog_display_mode'] = array(
    '#type' => 'select',
    '#options' => vt_commerce_catalog_get_view_mode('node'),
    '#title' => t('Select the node view mode that the catalog should use'),
    '#description' => t('You may need to configure the selected node view mode in the content types admin pages to adjust the fields and formats'),
    '#default_value' => variable_get('vt_commerce_catalog_display_mode', 'catalog_grid'),
  );

  return system_settings_form($form);
}