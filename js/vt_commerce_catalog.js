jQuery(document).ready(function ($) {
  
  // only process if there is catalog available
  if ($('#vt-catalog').length != 0) {
    var target = $('#vt-catalog'), item = target.find('.node');

    // break early if there is no item found
    if (item.length == 0) {
      return false;
    }
    
    // function for modifying the catalog grid mode   
    if (target.hasClass('grid')) {
      var itemWidth = item.width(), parentWidth = target.width();
      
      // estimate how many item can fit a single row
      var itemCanFit = Math.floor(parentWidth / itemWidth);
      
      // mark the row
      var i = 1, key = itemCanFit, b = 0;
      item.each(function(){
        if ((b * itemCanFit) == $(this).index()) {
          $(this).addClass('catalog-row');
          b++;
        }
      });
      
      // calculate the height and find the tallest
      var catalogRow = $('.catalog-row');
      
      catalogRow.each(function(){
        rowIndex = $(this).index();
        row = item.slice(rowIndex, itemCanFit + rowIndex);
        
        // reset the row height and assign the first row element
        // height
        rowHeight = row.eq(0).height();
        
        row.each(function() {
          if (rowHeight < $(this).height()) {
            rowHeight = $(this).height();
          }
        }).height(rowHeight);
      });
      
    }
    
  }
});