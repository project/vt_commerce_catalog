<?php

function vt_commerce_catalog_menu_admin_settings($form, &$form_state) {
  $vocabs = vt_commerce_catalog_get_vocabulary(variable_get('vt_commerce_catalog_display_product_type', array('product_display')));
  $form['vocabs'] = array(
    '#type' => 'value',
    '#value' => $vocabs,
  );
	// basic configuration
	$form['basic'] = array(
    '#type' => 'fieldset',
    '#weight' => 6,
    '#title' => t('Basic Configuration'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['basic']['vt_commerce_catalog_menu_vocabulary'] = array(
    '#type' => 'select',
    '#title' => t('Vocabulary allowed'),
    '#options' => $vocabs,
    '#multiple' => TRUE,
    '#description' => t('Select all the allowed vocabulary, you can select multiple vocabulary by ctrl + click on the selectable options.
    										 This module will build menu entry and Drupal block for each of the selected vocabularies'),
    '#default_value' => variable_get('vt_commerce_catalog_menu_vocabulary', array()),
  );

  $form['basic']['vt_commerce_catalog_menu_reset'] = array(
    '#type' => 'checkbox',
    '#title' => t('Reset Menu'),
    '#description' => t('Warning, this will delete all the menu created previously by this module, any modification / changes made in the menu created by this module will
    										 be removed. You cannot undo this process.'),
    '#default_value' => FALSE,
  );

  $form['basic']['vt_commerce_catalog_menu_expanded'] = array(
    '#type' => 'checkbox',
    '#title' => t('Always Expand Menu'),
    '#description' => t('Always expand menu, this is useful for displaying the sub-level menu'),
    '#default_value' => variable_get('vt_commerce_catalog_menu_expanded', TRUE),
  );

  $form['#submit'][] = 'vt_commerce_catalog_menu_admin_settings_submit';

  return system_settings_form($form);
}

function vt_commerce_catalog_menu_admin_settings_submit($form, &$form_state) {


  if (!empty($form_state['values']['vt_commerce_catalog_menu_reset'])) {
    $vids = vt_commerce_catalog_menu_load_record();
    foreach ($vids as $key => $value) {
      $vocab_object = new stdClass();
      $vocab_object->vid = $key;
      $menu_name = vt_commerce_catalog_build_menu_name($vocab_object);
      menu_delete(array('menu_name' => $menu_name));
      vt_commerce_catalog_menu_delete_record($vids, $vocab_object->vid, FALSE);
    }
  }


  if (!empty($form_state['values']['vt_commerce_catalog_menu_vocabulary'])) {
    $vocab_names = $form_state['values']['vt_commerce_catalog_menu_vocabulary'];
    $existing_menus = menu_get_menus(FALSE);
    $children = $form_state['values']['vt_commerce_catalog_menu_show_children'];
    vt_commerce_catalog_menu_rebuild_menu($vocab_names, 'catalog', $existing_menus, $children);
  }
}