jQuery(document).ready(function ($) {
  
  // slide up and down when clicked
  if ($('.basic_filter').length != 0 && $('.vtcc-basic-filter-js').length != 0) {
    var source = $('.vtcc-basic-filter-js').parent();
    var target = $('.basic_filter').parent().parent();

    source.click(function() {
      target.slideToggle();
      $('.vtcc-basic-filter-js').toggleClass('open');
    });
    
    // hide the content on first load
    // source.click();
  } 
});