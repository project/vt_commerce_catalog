jQuery(document).ready(function ($) {
  
  // slide up and down when clicked
  if ($('.advanced_filter').length != 0 && $('.vtcc-advanced-filter-js').length != 0) {
    var source = $('.vtcc-advanced-filter-js').parent();
    var target = $('.advanced_filter').parent().parent();

    source.click(function() {
      target.slideToggle();
      $('.vtcc-advanced-filter-js').toggleClass('open');
    });
    
    // hide the content on first load
    // source.click();
  } 
});