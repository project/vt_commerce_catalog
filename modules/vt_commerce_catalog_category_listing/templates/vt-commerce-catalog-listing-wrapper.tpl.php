<div class="vt-catalog-category">
  <?php if (!empty($image)) : ?>
    <?php print render($image); ?>
  <?php endif; ?>

  <?php if (!empty($parent_link)) : ?>
    <h2 class="title catalog category"><?php print $parent_link?></h2>
  <?php endif;?>

  <?php if (!empty($childtags)) : ?>
    <div class="vt-catalog-category-childtags">
    <?php print $childtags;?>
    </div>
  <?php endif; ?>

</div>