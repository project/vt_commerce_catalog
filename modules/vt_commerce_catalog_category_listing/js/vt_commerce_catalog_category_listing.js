jQuery(document).ready(function ($) {
  
  $('.category-vid').each(function(){
    var target = $(this).find('.vt-catalog-category');
    // function for equalizing height per row 
    if (target.length != 0) {
      resizeRowHeight(target);
    }
  });

  
  function resizeRowHeight(target) {
    var itemWidth = target.width(), parentWidth = target.parent().width();
    
    // estimate how many item can fit a single row
    var itemCanFit = Math.floor(parentWidth / itemWidth);

    // mark the row
    var b = 0;
    target.each(function(){
           
      if ((b * itemCanFit) == target.index($(this))) {
        $(this).addClass('category-row').prev('.vt-catalog-category').addClass('category-endrow');
        b++;
      }
    });
    
    // calculate the height and find the tallest
    var catalogRow = $('.category-row');
    
    catalogRow.each(function(){
      rowIndex = $(this).index();
      row = target.slice(rowIndex, itemCanFit + rowIndex);
      rowHeight = 0;
      row.each(function() {
        if (rowHeight < $(this).height()) {
          rowHeight = $(this).height();
          
        }
      }).height(rowHeight);
    });
  }
  
});